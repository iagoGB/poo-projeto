package br.com.appevento;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.SQLException;

import org.junit.Test;

import br.com.appevento.endpoint.UsuarioEndpoint;
import br.com.appevento.model.Usuario;
import br.com.appevento.repository.RepositorioUsuario;
import br.com.appevento.exception.RecursoNaoEncontradoException;

public class RepositorioUsuarioTeste {

	@Test
	public void InserirUsuarioComSucessoTeste () {
		RepositorioUsuario udao = new RepositorioUsuario();
		Date date = new Date(90,9,15);
		
		Usuario u = new Usuario (
			9905,
			"Tidinha",
			"Secretária Executiva",
			9,
			date
		);
		//Incluindo um novo usuario no banco com sucesso
		assertTrue(udao.salvar(u));
	} 
	
	@Test(expected= RecursoNaoEncontradoException.class)
	public void ConsultarUsuarioPorIdLancaExceptionTeste() throws RecursoNaoEncontradoException, SQLException {
		RepositorioUsuario ue = new RepositorioUsuario();
		//Consultando um usuario com id que não existe no banco é retornada uma exception
		ue.consultarPorId(99);
	}
	
	@Test (expected = RecursoNaoEncontradoException.class)
	public void DeletarUsuarioMalSucedidoTeste() throws RecursoNaoEncontradoException, SQLException {
		RepositorioUsuario udao = new RepositorioUsuario();
		//Tentando deletar um usuario que não existe e é retornada uma exception
		udao.deletar(50);
	} 
	
	@Test
	public void AtualizarUsuarioComSucessoTeste () throws RecursoNaoEncontradoException, SQLException {
		RepositorioUsuario udao = new RepositorioUsuario();
		Date date = new Date(99,02,22);
		
		Usuario u = new Usuario (
			8045,
			"Usuario Atualizado",
			"Programadora",
			12,
			date
		);
		//Incluindo um novo usuario no banco com sucesso
		assertTrue(udao.atualizar(u, 9));
	} 
	

}
