package br.com.appevento;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;

import org.junit.Test;

import br.com.appevento.endpoint.EventoEndpoint;
import br.com.appevento.endpoint.UsuarioEndpoint;
import br.com.appevento.model.Evento;
import br.com.appevento.model.Usuario;

public class InserirNovoUsuarioemTodosEventosExistentesTeste {

	@Test
	public void InserirDiretamenteTeste() {
		Date date = new Date (99,02,12);
		Usuario u = new Usuario(
					44890,
					"Inês Brasil",
					"Cantora",
					0,
					date
				);
		
		UsuarioEndpoint udao = new UsuarioEndpoint();
		udao.salvarUsuario(u);
	}

}
