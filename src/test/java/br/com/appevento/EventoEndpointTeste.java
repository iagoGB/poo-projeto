package br.com.appevento;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import br.com.appevento.connection.ConnectionFactory;
import br.com.appevento.endpoint.EventoEndpoint;
import br.com.appevento.model.Evento;

public class EventoEndpointTeste {
	
	@Test
	public void inserirEventoBemSucedidoTeste() {
		//Array de palestrantes
		String p1 = "Pa"; 
		String p2 = "les";
		String p3 = "tran";
		String p4 = "trante";
		ArrayList<String> array = new ArrayList<String>();
		array.add(p1); array.add(p2); array.add(p3); array.add(p4);
		
		//Objeto de acesso ao banco
		EventoEndpoint dao = new EventoEndpoint();
		
		//Dados do evento
		Date date = new Date(20090213);
		Evento evento;
		evento = new Evento ();
		evento.setCarga_horaria(2);
		evento.setCategoria_evento(4);
		evento.setLocalizacao("Caucaia");
		evento.setTitulo("Astronautas e Franceses");
		evento.setData_horario(date);
		evento.setPalestrante(array);
		//Se der certo salvar é retornado true, se não, é retornado false e o teste falha.
		assertTrue(dao.salveEvento(evento));
		
	}
	
}
	
