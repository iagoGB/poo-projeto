package br.com.appevento.repository;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



import br.com.appevento.connection.ConnectionFactory;
import br.com.appevento.model.Evento;

public class RepositorioEvento {
	public RepositorioEvento() {
		
	}

	public ArrayList<Evento> consultarTodos() {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<Evento> listaEvento = new ArrayList<Evento>();
		try {
			stmt = con.prepareStatement("SELECT * FROM evento ORDER BY evento_id DESC");
			rs= stmt.executeQuery();
			while (rs.next()) {
				Evento evento = new Evento();
				evento.setEvento_id(rs.getInt("evento_id"));
				evento.setCarga_horaria(rs.getDouble("carga_horaria"));
				evento.setData_horario(rs.getDate("data_evento"));
				evento.setLocalizacao(rs.getString("localizacao"));
				evento.setTitulo(rs.getString("titulo"));
				evento.setCategoria_evento(rs.getInt("categoria_evento"));
				evento.setPalestrante(getPalestrantes(evento.getEvento_id()));
				listaEvento.add(evento);
			}
			return listaEvento;	
		} catch (SQLException e) {
			throw new RuntimeException("Erro ao conectar",e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
	}

	public ArrayList<Evento> consultarPorId(int id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<Evento> listaEvento = new ArrayList<Evento>();
		try
		{
			Evento evento = new Evento();
			stmt = con.prepareStatement("SELECT * FROM evento WHERE evento_id = ?");
			stmt.setInt(1, id);
			rs= stmt.executeQuery();
			if (rs.next()) {
				//Jogue os dados para o objeto Evento
				evento.setEvento_id(rs.getInt("evento_id"));
				evento.setCarga_horaria(rs.getDouble("carga_horaria"));
				evento.setData_horario(rs.getDate("data_evento"));
				evento.setLocalizacao(rs.getString("localizacao"));
				evento.setTitulo(rs.getString("titulo"));
				evento.setCategoria_evento(rs.getInt("categoria_evento"));
				listaEvento.add(evento);
		}
			//Percorrer a tabela de palestrantes e insira-os em um array no objeto Evento
			evento.setPalestrante(getPalestrantes(id));
			return listaEvento;
		} catch (SQLException e) {
			throw new RuntimeException("Erro ao solicitar evento",e);
		} finally {
			ConnectionFactory.closeConnection(con,stmt,rs);
		}
	}
	
	public boolean salvar(Evento evento) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String[] returnId = { "evento_id" };
		try 
		{
			String sql =  "INSERT INTO evento (" + 
						"titulo, data_evento, carga_horaria, localizacao, categoria_evento)" + 
						"VALUES (?, ?, ?, ?, ?);";
			stmt = con.prepareStatement( sql, returnId);
			stmt.setString(1, evento.getTitulo());
			stmt.setDate(2, evento.getData_horario());
			stmt.setDouble(3, evento.getCarga_horaria());
			stmt.setString(4, evento.getLocalizacao());
			stmt.setInt(5, evento.getCategoria_evento());
			stmt.executeUpdate();
			try
			{
				//Insira os palestrantes do evento
				rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					evento.setEvento_id(rs.getInt(1));
					inserePalestrante(evento.getEvento_id(), evento.getPalestrante());
				}
			} catch (Exception e) {
				System.out.println("Erro:"+ e);
			}
			RepositorioUsuario udao = new RepositorioUsuario();
			udao.insereUsuariosEmNovoEvento( evento.getEvento_id());
			System.out.println( "Evento adicionado:" + evento.toString());
			return true;
		} catch (SQLException e) {
			System.err.println("erro ao adicionar evento:"+ e);
			return false;	
		} finally {
			ConnectionFactory.closeConnection(con,stmt,rs);
		}
	}
	
	public void atualizar(int id, Evento evento) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.prepareStatement("UPDATE evento SET " + 
					"titulo = ?, data_evento = ?, carga_horaria = ?, localizacao = ?, categoria_evento = ?, criado_em = ?, atualizado_em = ? "
					+ "WHERE evento_id =?");
			
			stmt.setString(1, evento.getTitulo());
			stmt.setDate(2,  (Date) evento.getData_horario());
			stmt.setDouble(3, evento.getCarga_horaria());
			stmt.setString(4, evento.getLocalizacao());
			stmt.setInt(5, evento.getCategoria_evento());
			stmt.setInt(8, id);
			
			stmt.executeUpdate();
			System.out.println("Parece que funcionou");
		} catch (SQLException e) {
			throw new RuntimeException("Foi diferente o erro", e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}			
	}

	public boolean deletar(int id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		//Delete um evento único
			try {
				deletarPalestrante(id);
				String sql = " DELETE FROM evento WHERE evento_id = ?";
				stmt = con.prepareStatement(sql);
				stmt.setInt(1, id);
				stmt.executeUpdate();
				
				return true;
			} catch (SQLException e) {
				System.err.println("Erro:"+ e);
				return false;
			} finally {
				ConnectionFactory.closeConnection(con, stmt);
			}	
	}
		
	
	//Liste o(s) palestrante(s) do evento
		public ArrayList<String> getPalestrantes(Integer id){
			Connection con = ConnectionFactory.getConnection();
			PreparedStatement stmt = null;
			ResultSet rs = null;
			ArrayList<String> listaPalestrante = new ArrayList<String>();
			
			try {
				stmt = con.prepareStatement("SELECT palestrante FROM evento AS e, evento_palestrante AS ep WHERE ep.evento_id = e.evento_id AND e.evento_id = ?");
				stmt.setInt(1, id);
				rs= stmt.executeQuery();
				while (rs.next()) {
					listaPalestrante.add(rs.getString("palestrante"));
				}
				return listaPalestrante;
			} catch (SQLException e) {
				System.err.println("Erro ao solicitar palestrante(s):" + e);
				return null;
			}
		}
		
	
		//Insira os palestrantes de um evento
		public void inserePalestrante(Integer id,List<String> pList) {
			Connection con = ConnectionFactory.getConnection();
			PreparedStatement stmt = null;
			ResultSet rs = null;
			String sql = "INSERT INTO evento_palestrante(evento_id, palestrante) VALUES (?,?)";
			try {
				for (String p: pList) {
					stmt = con.prepareStatement(sql);
					stmt.setInt(1,id);
					stmt.setString(2,p);
					stmt.executeUpdate();
				}
				
			} catch (SQLException e) {
				System.err.println("Erro ao inserir o(s) palestrante(s): "+ e);
			} finally {
				
			}
		}

		
		ArrayList<?> consultarPorId() {
			// TODO Auto-generated method stub
			return null;
		}

		
		void deletar() {
			// TODO Auto-generated method stub
			
		}
		
		public boolean deletarPalestrante(int id) {
			Connection con = ConnectionFactory.getConnection();
			PreparedStatement stmt = null;
			ResultSet rs = null;
			try {
				String sql = "DELETE  FROM evento_palestrante where evento_id = ?";
				stmt = con.prepareStatement(sql);
				stmt.setInt(1,id);
				stmt.executeUpdate();
				return true;
			} catch (SQLException e) {
				System.err.println("Erro ao deletar palestrantes: "+ e);
				return false;
			} finally {
				ConnectionFactory.closeConnection(con, stmt);
			}	
		}

		void atualizar() {
			// TODO Auto-generated method stub
			
		}

}
