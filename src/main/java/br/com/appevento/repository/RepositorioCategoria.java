package br.com.appevento.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import br.com.appevento.connection.ConnectionFactory;
import br.com.appevento.model.Categoria;

public class RepositorioCategoria  {

	
	public ArrayList<Categoria> consultarTodos() {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<Categoria> listaCategoria = new ArrayList<Categoria>();
		
		try {
			String sql = "SELECT * FROM categoria";
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();
			
			while(rs.next()) {
				Categoria categoria = new Categoria();
				categoria.setCategoria_id(rs.getInt("categoria_id"));
				categoria.setNome(rs.getString("nome"));
				listaCategoria.add(categoria);
			}
			return listaCategoria;
		} catch (SQLException e) {
			throw new RuntimeException("Erro ao conectar",e);
		} finally {
			ConnectionFactory.closeConnection(con,stmt,rs);
		}
	}

}
