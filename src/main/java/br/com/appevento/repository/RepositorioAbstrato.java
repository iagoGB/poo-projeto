package br.com.appevento.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import br.com.appevento.connection.ConnectionFactory;

public abstract class RepositorioAbstrato {
	protected Connection con = ConnectionFactory.getConnection();
	protected PreparedStatement stmt = null;
	protected ResultSet rs = null;
	
	abstract ArrayList<?>consultarTodos();
	abstract ArrayList<?>consultarPorId();
	abstract void atualizar();
	abstract void deletar();
	
}
