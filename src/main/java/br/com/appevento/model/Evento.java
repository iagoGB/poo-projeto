package br.com.appevento.model;

import java.sql.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;


public class Evento {
	
		private int evento_id;
		private int categoria_evento;
		private String titulo;
		private List<String> palestrante;
		@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
		private Date data_horario;
		private String localizacao;
		private double carga_horaria;
		
		public Evento() {
			
		}
		
		public Evento(int categoria_evento, String titulo, List<String> palestrante, Date data_horario,
				String localizacao, double carga_horaria) {
			this.categoria_evento = categoria_evento;
			this.titulo = titulo;
			this.palestrante = palestrante;
			this.data_horario = data_horario;
			this.localizacao = localizacao;
			this.carga_horaria = carga_horaria;
		}
		
		public int getEvento_id() {
			return evento_id;
		}
		
		public void setEvento_id(int evento_id) {
			this.evento_id = evento_id;
		}
		
		public int getCategoria_evento() {
			return categoria_evento;
		}
		
		public void setCategoria_evento(int categoria_evento) {
			this.categoria_evento = categoria_evento;
		}
		
		public String getTitulo() {
			return titulo;
		}
		
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		
		public List<String> getPalestrante() {
			return palestrante;
		}
		
		public void setPalestrante(List<String> palestrante) {
			this.palestrante = palestrante;
		}
		
		public Date getData_horario() {
			return data_horario;
		}
		
		public void setData_horario(Date data_horario) {
			this.data_horario = data_horario;
		}
		
		public String getLocalizacao() {
			return localizacao;
		}
		
		public void setLocalizacao(String localizacao) {
			this.localizacao = localizacao;
		}
		
		public double getCarga_horaria() {
			return carga_horaria;
		}
		
		public void setCarga_horaria(double carga_horaria) {
			this.carga_horaria = carga_horaria;
		}

		@Override
		public String toString() {
			return "Evento [evento_id=" + evento_id + ", categoria_evento=" + categoria_evento + ", titulo=" + titulo
					+ ", palestrante=" + palestrante + ", data_horario=" + data_horario + ", localizacao=" + localizacao
					+ ", carga_horaria=" + carga_horaria + "]";
		}				
}
