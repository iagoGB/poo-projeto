package br.com.appevento.model;

import java.sql.Date;

public class Usuario {
	
	private int usuario_id;
	    private int cpf;
	    private String nome;
	    private String profissao;
	    private double carga_horaria;
	    private Date data_ingresso;
	    
	    public Usuario () {
	    	
	    }
	    
	    
	    
		public Usuario (
				int cpf, 
				String nome, 
				String profissao,
				double carga_horaria,
				Date data_ingresso
		) {
			this.cpf = cpf;
			this.nome = nome;
			this.profissao = profissao;
			this.carga_horaria = carga_horaria;
			this.data_ingresso = data_ingresso;
		}



		public int getUsuario_id() {
			return usuario_id;
		}
		public void setUsuario_id(int usuario_id) {
			this.usuario_id = usuario_id;
		}
		public int getCpf() {
			return cpf;
		}
		public void setCpf(int cpf) {
			this.cpf = cpf;
		}
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public String getProfissao() {
			return profissao;
		}
		public void setProfissao(String profissao) {
			this.profissao = profissao;
		}
		public double getCarga_horaria() {
			return carga_horaria;
		}
		public void setCarga_horaria(double carga_horaria) {
			this.carga_horaria = carga_horaria;
		}
		public Date getData_ingresso() {
			return data_ingresso;
		}
		public void setData_ingresso(Date data_ingresso) {
			this.data_ingresso = data_ingresso;
		}
		
		 @Override
			public String toString() {
				return "Usuario [usuario_id=" + usuario_id + ", cpf=" + cpf + ", nome=" + nome + ", profissao=" + profissao
						+ ", carga_horaria=" + carga_horaria + ", data_ingresso=" + data_ingresso + "]";
			}
}
