package br.com.appevento.endpoint;


import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.appevento.model.Categoria;
import br.com.appevento.repository.RepositorioCategoria;

@RestController
@RequestMapping("/categoria")
public class CategoriaEndpoint {
	
	RepositorioCategoria repCategoria = new RepositorioCategoria();
	
	@GetMapping
	public ArrayList<Categoria> getCategoria(){
		return repCategoria.consultarTodos();
	}
}
